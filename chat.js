var app  = require("express")();
var http = require('http').Server(app);
var io   = require("socket.io")(http);
var bodyParser = require('body-parser');
var socketsUsers = [];

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.post('/newMessage', function(message, response){
    var body = message.body;

    if ("undefined" !== typeof body.signature) delete body.signature;
    if ("undefined" !== typeof body.state) delete body.state;
    console.log(body);

    io.to('chat-id-'+body.chat_id).emit('newMessage', body);
    // console.log(io.sockets.adapter.rooms);

    response.status(200).send('OK');
});

app.post('/changedChat', function(message, response){
    var body = message.body;
    console.log(body);
    io.to('chat-id-'+body.id).emit('changedChat', body);
    if (body.isNew) io.to('host-id-'+body.host_id).emit('newChat', body);

    response.status(200).send('OK');
});

app.post('/userJoinChat', function(message, response){
    var body = message.body;
    io.to('host-id-' + body.chat.host_id).emit('userJoinChat', body);

    response.status(200).send('OK');
});

app.post('/getUsers', function(message, response){ //return all users joined to host
    var host = message.body;
    var clients = io.sockets.adapter.rooms['host-id-' + host.id];
    var users = [];
    if ('undefined' !== typeof clients)
        for (var socket in clients.sockets)
            users.push(socketsUsers[socket]);

    response.status(200).send(users);
});

app.post('/getHosts', function(message, response){ //return all hosts to which user is joined
    var userId = message.body.userId;
    hosts = [];
    for (var socket in socketsUsers)
        if (userId === socketsUsers[socket].id) {
            hosts = socketsUsers[socket].hosts;
            break;
        }
    response.status(200).send(hosts);
});

app.post('/changedMessagesStatus', function(data, response){
    var body = data.body;
    io.to('chat-id-' + body.message.chat_id).emit('changedMessagesStatus', body);

    response.status(200).send('OK');
});

io.on('connection', function(socket) {
    socket.on('join', function(data) {
        socket.join('chat-id-'+data.chatId);
        if ("undefined" != typeof data.userId)
        	console.log('user ' + data.userId + ' joined to chat-id-' + data.chatId);
        else 
        	console.log('visitor ' + data.visitorId + ' joined to chat-id-' + data.chatId);
    });

    socket.on('leave', function(data){
        socket.leave('chat-id-'+data.chatId);
        if ("undefined" != typeof data.userId)
            console.log('user ' + data.userId + ' left chat-id-' + data.chatId);
        else 
            console.log('visitor left chat-id-' + data.chatId);
    })

    socket.on('userJoinToHost', function(data) {
        // console.log(data);
        socketsUsers[socket.id] = {
            id: data.userId,
            hosts: data.hosts
        }
        io.to('pages').emit('userConnect', socketsUsers[socket.id]);
        for (var i in data.hosts) {
            var hostId = data.hosts[i];
            socket.join('host-id-'+hostId);
            console.log('user ' + data.userId + ' joined to ' + 'host-id-'+hostId);
        }
    });

    socket.on('userJoinPages', function(data) {
        socket.join('pages');
        console.log('user ' + data.userId + ' joined to pages');
    })

    socket.on('disconnect', function() {
        var userData = socketsUsers[socket.id];
        if('undefined' !== typeof userData) {
            delete socketsUsers[socket.id];
            io.to('pages').emit('userDisconnect', userData);
        }
    });
});

http.listen(1584, function() {
    console.log("Listening on 1584");
});
